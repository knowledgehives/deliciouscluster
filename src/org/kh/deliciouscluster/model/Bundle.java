package org.kh.deliciouscluster.model;

/**
 * Bundle
 * 
 * @author David Czarnecki, skruk
 * @version $Id: Bundle.java,v 1.2 2007/04/18 09:14:23 skruk Exp $
 * @since 1.9
 */
public class Bundle {

	private String name;
	private String[] tags;

	/**
	 * Create a new bundle with a bundle name and a space separated list of tags
	 * 
	 * @param _name
	 *            Bundle name
	 * @param _tags
	 *            List of tags
	 */
	public Bundle(String _name, String... _tags) {
		this.name = _name;
		this.tags = _tags;
	}

	/**
	 * Get the name of this bundle
	 * 
	 * @return Name of this bundle
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of this bundle
	 * 
	 * @param _name
	 *            Name of this bundle
	 */
	public void setName(String _name) {
		this.name = _name;
	}

	/**
	 * Get the space separated list of tags for this bundle
	 * 
	 * @return Space separated list of tags
	 */
	public String[] getTags() {
		return tags;
	}

	/**
	 * Set the list of tags for this bundle
	 * 
	 * @param _tags
	 *            List of tags for this bundle
	 */
	public void setTags(String... _tags) {
		this.tags = _tags;
	}

	/**
	 * Object as name:tags string
	 * 
	 * @return name:tags
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(name).append(":").append(tags); //$NON-NLS-1$

		return result.toString();
	}
}

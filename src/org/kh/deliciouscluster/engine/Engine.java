package org.kh.deliciouscluster.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.kh.deliciouscluster.model.Post;
import org.kh.deliciouscluster.model.Tag;
import org.kh.deliciouscluster.model.Term;
import org.kh.deliciouscluster.process.Bundles;
import org.kh.deliciouscluster.process.ClusterNaming;
import org.kh.deliciouscluster.process.Clustering;
import org.slf4j.LoggerFactory;

import del.icio.us.DeliciousNotAuthorizedException;
import del.icio.us.DeliciousNotAvailableException;

public class Engine  {

	/**
	 * Static logger for this class
	 */
	public static org.slf4j.Logger logger = LoggerFactory.getLogger(Engine.class);
	//--processors
	Bundles bundleprocessor;
	Clustering similarityprocessor;
	ClusterNaming namingprocessor;
	
	/**
	 * del.icio.us source API wrapper 
	 */
	protected Source source_;
	/**
	 * Random numbers generator for generating ID values
	 */
	public static final Random random = new Random(new Date().getTime());
	private Map<String, Collection<Post>> cache;

	public List<Term> getTags() throws DeliciousNotAuthorizedException, DeliciousNotAvailableException {
		List<Tag> deltags = this.source_.getTags();
		List<Term> result = this.getTerms(deltags);
		
		result = this.bundleprocessor.process(result, this.source_.getBundles());
		result = this.similarityprocessor.process(result);
		result = this.namingprocessor.process(result);
			
		return result;
	}

	/**
	 * Prepares a list of {@link Term} out of a collection of filtered tags
	 * @param _tags
	 * @return
	 */
	public List<Term> getTerms(Collection<Tag> _tags){
		// ------------ puting data to map --------------------------
		
		List<Term> lmappables = new ArrayList<Term>(); 
		int i = 0;
		for(Tag tag : _tags) {
			Term mtag = new Term(tag.getTag(), (double)tag.getCount());
			lmappables.add(mtag);
			mtag.setOrder(i++);
		}
		
		return lmappables;
	}


	
	/**
	 * Returns cached map of posts per tag
	 * @param label by which to filter
	 * @return
	 */
	public synchronized Collection<Post> getPostsPerTag(String _tag){

		if (this.cache.size() == 0) {
			List<Post> _posts = null;
			int cnt = 0;

			while (_posts == null && cnt < 10) { 
				try {
					_posts = this.source_.getAllPosts();
				}catch(Exception noex) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						//no info - some sleeping problems only
					}
				}finally {
					cnt++;
				}
			}
		
			if (_posts != null){
				for (Post post : _posts) 
					for (String tag : post.getTags()) {
						Collection<Post> cpost = this.cache.get(tag);
						
						if (cpost == null) {
							cpost = new HashSet<Post>();
							this.cache.put(tag, cpost);
						}
						cpost.add(post);
					}
				this.cache.put(null, _posts);
			}
		}
		
		Collection<Post> result = this.cache.get(_tag);
		if(result == null)
			result = new HashSet<Post>();
	
		
		return Collections.unmodifiableCollection(result);
	}
	

	public Engine(Source source){
		this.source_ = source;
		this.bundleprocessor = new Bundles();
		this.similarityprocessor = new Clustering();
		this.namingprocessor = new ClusterNaming();		
		this.cache = new HashMap<String, Collection<Post>>();
	}

}
package org.kh.deliciouscluster.process;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kh.deliciouscluster.model.Bundle;
import org.kh.deliciouscluster.model.Term;

import del.icio.us.DeliciousNotAuthorizedException;
import del.icio.us.DeliciousNotAvailableException;

/**
 * Responsible for getting bundles from delicions, using them as clusters and
 * removing clustered that way tags
 * 
 * @author sebastiankruk
 * 
 */
public class Bundles {

	public List<Term> process(List<Term> tags, List<Bundle> bundles)
			throws DeliciousNotAuthorizedException,
			DeliciousNotAvailableException {
		List<Term> result = Term.get(tags.toArray(new Term[tags.size()]));
		Set<Term> usedTags = new HashSet<Term>();

		for (Bundle bundle : bundles) {
			Set<Term> children = this.retrieveTags(result, bundle.getTags());
			usedTags.addAll(children);

			result.add(new Term(bundle.getName(), null, children
					.toArray(new Term[children.size()])));
		}

		result.removeAll(usedTags);
		return result;
	}

	/**
	 * For each tag assigned only with a name this method tries to find matching
	 * one in the list of given base set
	 * 
	 * @param baseset
	 * @param tagsnames
	 * @return
	 */
	protected Set<Term> retrieveTags(List<Term> baseset, String... tagsnames) {
		Set<Term> result = new HashSet<Term>();

		// TODO try to improve the efficiency of this algorithm
		for (String tagname : tagsnames)
			for (Term term : baseset)
				if (term.getTerm().equals(tagname))
					result.add(term);

		return result;
	}

}

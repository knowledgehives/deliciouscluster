package org.kh.deliciouscluster.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kh.deliciouscluster.model.Term;

import del.icio.us.DeliciousNotAuthorizedException;
import del.icio.us.DeliciousNotAvailableException;

/**
 * @author sebastiankruk
 * 
 */
public class Clustering {

	public List<Term> process(List<Term> tags)
			throws DeliciousNotAuthorizedException,
			DeliciousNotAvailableException {
		List<Term> result = new ArrayList<Term>();

		if (tags.size() > 0) {
			int n = 3;

			Term[] astags = tags.toArray(new Term[tags.size()]);
			Collection<Float> ssim = new ArrayList<Float>();
			Double averg = 0.0;

			// -- calculating similarities
			for (int i = 0; i < astags.length - 1; i++)
				for (int j = i + 1; j < astags.length; j++) {
					float f = astags[i].similarity(astags[j]);
					ssim.add(f);
					averg += f;
				}

			// -- average similarity
			averg /= ssim.size();

			// -- calculating standard deviation
			Double dev = 0.0;
			for (Float f : ssim)
				dev += (f - averg) * (f - averg);
			dev /= ssim.size();
			dev = Math.sqrt(dev);

			// -- threshold: averg + n*dev
			float threshold = (float) (averg + n * dev); // XXX
			for (int i = 0; i < astags.length; i++)
				astags[i].setThreshold(threshold);

			// -- objects required by second round
			Set<Term> clusters = new HashSet<Term>();
			Set<Term> nonclusters = new HashSet<Term>();

			// -- do first round of clustering
			Term tmt = astags[0];
			result.add(tmt);
			nonclusters.add(tmt);

			for (int i = 1; i < astags.length; i++) { // -- for each remaining
														// tag
				Term tag = astags[i];
				boolean isnewclusterseed = true;

				for (Term cluster : result)
					// -- check each cluster
					if (cluster.similar(tag)) { // if the tag seems similar to
												// the cluster
						// fos.println("!"+cluster+"<-"+tag);
						cluster.addChildren(tag); // -- put it as a child

						if (cluster.getChildrenSize() == 1) { // if this is a
																// new cluster
							cluster.clearChildren();
							Term tmtag = new Term(cluster); // -- we need to
															// ensure copy
															// creation here
							cluster.addChildren(tmtag, tag); // -- move the
																// previous
																// object down
																// the tree
							cluster.setTag(""); // -- clear the tag name - make it a new cluster //$NON-NLS-1$
							clusters.add(cluster);
						}// -- if new cluster

						isnewclusterseed = false;
					}// if similar

				if (isnewclusterseed) {
					result.add(tag);
					nonclusters.add(tag);
				}

			}// for each tag

			nonclusters.removeAll(clusters);

			// -- do second round of clustering
			astags[0].setThreshold(threshold / 2);
			Set<Term> toremove = new HashSet<Term>();

			for (Term tag : nonclusters) {
				boolean clustered = false;

				for (Term cluster : clusters)
					if (cluster.similar(tag)) {
						// DEBUG fos.println(cluster + " ~ " + tag );
						cluster.addChildren(tag);
						clustered = true;
					}

				if (clustered)
					toremove.add(tag);
			}// -- for each non-clustered

			// Date finish2date = new Date();

			nonclusters.removeAll(toremove);

			// *****************************************************************
			// Now do the fine grained clustering on all clusters with size > 3
			// *****************************************************************
			for (Term cluster : clusters)
				if (cluster.getChildrenSize() > 3 && cluster.getDepth() < 10) {
					List<Term> newchild = this.process(cluster.getChildren());

					cluster.removeChildren(cluster.getChildren().toArray(
							new Term[cluster.getChildrenSize()]));
					cluster.addChildren(newchild.toArray(new Term[newchild
							.size()]));
				}

			result.removeAll(toremove);

			// --- we need to check if stupid clusters (with only one element)
			// were not created
			List<Term> lcsl = new ArrayList<Term>();
			for (Term cluster : clusters) {
				this.correctSmallClusters(cluster, lcsl);
			}
			// -- add upgraded clusters to the main list of results
			result.addAll(lcsl);

		}

		return result;
	}

	/**
	 * Corrects clusters with only one subcluster element - by moving this
	 * sub-cluster element to the list of clusters
	 * 
	 * @param cluster
	 * @param clusters
	 */
	protected void correctSmallClusters(Term cluster, List<Term> clusters) {
		// -- try to correct children first
		List<Term> lcsl = new ArrayList<Term>();
		for (Term child : cluster.getChildren())
			this.correctSmallClusters(child, lcsl);

		if (lcsl.size() > 0)
			cluster.addChildren(lcsl.toArray(new Term[lcsl.size()]));

		if (cluster.getChildrenSize() > 0 && cluster.getChildrenSize() < 2) {
			Term child = cluster.getChildren().get(0);

			child.setDepth(cluster.getDepth());
			clusters.add(child);
			cluster.removeChildren(child);
		}// -- if cluster too small

	}
}
